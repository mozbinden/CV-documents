# This project contains the documents related to the additional documents with Morgane Zbinden CV.
It contains the files related to mandatory scolarity, Federal Maturity and EPFL Bachelor and Master degrees.
You can also find my First Aid certificate. The Ethics certificates are also available.

Each file begins with the related year of obtention.

Please, find also [here](https://lalepiote.ch/), a website I created for my sister.
